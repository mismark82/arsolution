﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AR_CRM_App.Models
{
    public class Cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cliente()
        {
            this.Offerta = new HashSet<Offerta>();
        }

        public int Id { get; set; }
        public int IdCliente { get; set; }
        public string RagSoc1 { get; set; }
        public string RagSoc2 { get; set; }
        public string Indirizzo { get; set; }
        public string CAP { get; set; }
        public string Localita { get; set; }
        public string Prov { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string PIva { get; set; }
        public string CF { get; set; }
        public string EMail { get; set; }
        public string HomePage { get; set; }
        public string S_RagSoc1 { get; set; }
        public string S_RagSoc2 { get; set; }
        public string S_Indirizzo { get; set; }
        public string S_CAP { get; set; }
        public string S_Localita { get; set; }
        public string S_Prov { get; set; }
        public string Note { get; set; }
        public string CodAge { get; set; }
        public Nullable<int> CodPag { get; set; }
        public Nullable<bool> Mitt { get; set; }
        public string Cell { get; set; }
        public Nullable<int> IDTip { get; set; }
        public Nullable<int> BanApp { get; set; }
        public string IBAN { get; set; }
        public string CCN { get; set; }
        public string CIN { get; set; }
        public Nullable<int> ZonCod { get; set; }
        public Nullable<System.DateTime> DatUltMod { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Offerta> Offerta { get; set; }
    }
}