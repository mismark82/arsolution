﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AR_CRM_App.Models
{
    public class VoceOfferta
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> IdOfferta { get; set; }
        public string TipRig { get; set; }
        public Nullable<int> IdArticolo { get; set; }
        public string Titolo { get; set; }
        public string DesOff { get; set; }
        public Nullable<double> Qta { get; set; }
        public Nullable<decimal> Prezzo { get; set; }
        public Nullable<decimal> C_Prezzo { get; set; }
        public Nullable<decimal> Listino { get; set; }
        public Nullable<decimal> Costo { get; set; }
        public Nullable<int> IDForn { get; set; }
        public Nullable<System.DateTime> DataArrivo { get; set; }
        public Nullable<System.DateTime> DataCons { get; set; }
        public Nullable<int> OrdVoce { get; set; }
        public Nullable<bool> ScoPer { get; set; }
        public Nullable<bool> Conf { get; set; }
        public string PathFoto { get; set; }
        public Nullable<int> IDMacchina { get; set; }
        public string UM { get; set; }
        public Nullable<int> GruMer { get; set; }
        public Nullable<double> BatLun { get; set; }
        public Nullable<double> BatLar { get; set; }
        public Nullable<double> BatSpe { get; set; }
        public Nullable<double> BatPS { get; set; }
        public Nullable<double> BatPreKG { get; set; }
        public Nullable<double> BatPerAum { get; set; }
        public Nullable<double> BatPreLav { get; set; }
        public string CodArt { get; set; }
        public string CodMarca { get; set; }
        public Nullable<double> Forecast { get; set; }
        public Nullable<bool> InAss { get; set; }
        public Nullable<int> NumMac { get; set; }
        public Nullable<double> NumOre { get; set; }
        public string IDReparto { get; set; }
        public Nullable<double> CostoOrario { get; set; }

        public virtual Articolo Articolo { get; set; }
        public virtual Offerta Offerta { get; set; }
    }
}