﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace AR_CRM_App.Models
{
    public class Articolo
    {
        //standard, da P7 ext
        public Articolo()
        {
            this.VociOfferta = new HashSet<VoceOfferta>();
        }

        [Key]
        public int Id { get; set; }
        public string cod_art { get; set; }
        public string Marca { get; set; }
        public string Modello { get; set; }
        public string Titolo { get; set; }
        public string DesOff { get; set; }
        public string DesPic { get; set; }
        public Nullable<bool> Acc { get; set; }
        public string Foto { get; set; }
        public Nullable<int> IDTipolog { get; set; }
        public Nullable<int> Durata { get; set; }
        public string IDMarche { get; set; }
        public Nullable<int> IDTipSrv { get; set; }
        public Nullable<double> ConvRifVal { get; set; }
        public Nullable<int> ConvRifID1 { get; set; }
        public Nullable<int> ConvRifID2 { get; set; }
        public Nullable<double> ConvMagVal { get; set; }
        public Nullable<int> ConvMagID1 { get; set; }
        public Nullable<int> ConvMagID2 { get; set; }
        public Nullable<double> X { get; set; }
        public Nullable<double> Y { get; set; }
        public Nullable<double> Z { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VoceOfferta> VociOfferta { get; set; }

        // custom

        public decimal Prezzo { get; set; }
    }
}