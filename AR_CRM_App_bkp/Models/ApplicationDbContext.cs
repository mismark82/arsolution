﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AR_CRM_App.Models
{
    // È possibile aggiungere dati del profilo per l'utente aggiungendo altre proprietà alla classe ApplicationUser. Per altre informazioni, vedere https://go.microsoft.com/fwlink/?LinkID=317594.
    

    public class ApplicationDbContext : IdentityDbContext<Utente>
    {

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            // Evita che faccia la pluralizzazione dei nomi tabella in inglese
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);

            // Custom IdentityFramework Table Names at Startup
            modelBuilder.Entity<Utente>().ToTable("Utenti").Property(p => p.Id).HasColumnName("Id");
            modelBuilder.Entity<IdentityUserRole>().ToTable("RuoliUtente");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("LoginUtente");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("AttestazioniUtente");
            modelBuilder.Entity<IdentityRole>().ToTable("Ruoli");

            // Nomi di tebelle custom per le entità custom del gestionale
            modelBuilder.Entity<Utilizzatore>().ToTable("Utilizzatori");
            modelBuilder.Entity<VoceOfferta>().ToTable("VociOfferta");
            modelBuilder.Entity<Offerta>().ToTable("Offerte");
            modelBuilder.Entity<Cliente>().ToTable("Clienti");
            modelBuilder.Entity<Articolo>().ToTable("Articoli");

        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<Articolo> Articoli { get; set; }
        public System.Data.Entity.DbSet<Utente> Utenti { get; set; }
        public System.Data.Entity.DbSet<Utilizzatore> Utilizzatori { get; set; }
        public System.Data.Entity.DbSet<Offerta> Offerte { get; set; }
        public System.Data.Entity.DbSet<Cliente> Clienti { get; set; }
    }
}