﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AR_CRM_App.Startup))]
namespace AR_CRM_App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
