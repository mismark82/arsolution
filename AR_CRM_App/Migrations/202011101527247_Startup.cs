namespace AR_CRM_App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Startup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articoli",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        cod_art = c.String(),
                        Marca = c.String(),
                        Modello = c.String(),
                        Titolo = c.String(),
                        DesOff = c.String(),
                        DesPic = c.String(),
                        Acc = c.Boolean(),
                        Foto = c.String(),
                        IDTipolog = c.Int(),
                        Durata = c.Int(),
                        IDMarche = c.String(),
                        IDTipSrv = c.Int(),
                        ConvRifVal = c.Double(),
                        ConvRifID1 = c.Int(),
                        ConvRifID2 = c.Int(),
                        ConvMagVal = c.Double(),
                        ConvMagID1 = c.Int(),
                        ConvMagID2 = c.Int(),
                        X = c.Double(),
                        Y = c.Double(),
                        Z = c.Double(),
                        Prezzo = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VociOfferta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdOfferta = c.Int(),
                        TipRig = c.String(),
                        IdArticolo = c.Int(),
                        Titolo = c.String(),
                        DesOff = c.String(),
                        Qta = c.Double(),
                        Prezzo = c.Decimal(precision: 18, scale: 2),
                        C_Prezzo = c.Decimal(precision: 18, scale: 2),
                        Listino = c.Decimal(precision: 18, scale: 2),
                        Costo = c.Decimal(precision: 18, scale: 2),
                        IDForn = c.Int(),
                        DataArrivo = c.DateTime(),
                        DataCons = c.DateTime(),
                        OrdVoce = c.Int(),
                        ScoPer = c.Boolean(),
                        Conf = c.Boolean(),
                        PathFoto = c.String(),
                        IDMacchina = c.Int(),
                        UM = c.String(),
                        GruMer = c.Int(),
                        BatLun = c.Double(),
                        BatLar = c.Double(),
                        BatSpe = c.Double(),
                        BatPS = c.Double(),
                        BatPreKG = c.Double(),
                        BatPerAum = c.Double(),
                        BatPreLav = c.Double(),
                        CodArt = c.String(),
                        CodMarca = c.String(),
                        Forecast = c.Double(),
                        InAss = c.Boolean(),
                        NumMac = c.Int(),
                        NumOre = c.Double(),
                        IDReparto = c.String(),
                        CostoOrario = c.Double(),
                        Articolo_Id = c.Int(),
                        Offerta_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Articoli", t => t.Articolo_Id)
                .ForeignKey("dbo.Offerte", t => t.Offerta_Id)
                .Index(t => t.Articolo_Id)
                .Index(t => t.Offerta_Id);
            
            CreateTable(
                "dbo.Offerte",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IDCliOff = c.Int(),
                        IDCliPic = c.Int(),
                        DataOff = c.DateTime(),
                        ProgOff = c.Int(),
                        Oggetto = c.String(),
                        Intro = c.String(),
                        Note = c.String(),
                        Saluti = c.String(),
                        Validita = c.DateTime(),
                        Installazione = c.String(),
                        ConsTempi = c.String(),
                        Accettata = c.Boolean(),
                        Rifiutata = c.Boolean(),
                        MotivoRif = c.String(),
                        PicNomPrev = c.String(),
                        PicNumPrev = c.Int(),
                        PicDataPrev = c.DateTime(),
                        C_Consegna = c.DateTime(),
                        C_SpedMez = c.String(),
                        C_Imballo = c.String(),
                        C_Pagamento = c.String(),
                        C_Banca = c.String(),
                        C_RifSig = c.String(),
                        C_Note = c.String(),
                        O_KitInstDes = c.String(),
                        O_KitInst = c.Decimal(precision: 18, scale: 2),
                        O_InstForfait = c.Boolean(),
                        O_InstTrasp = c.Decimal(precision: 18, scale: 2),
                        O_InstOrePrev = c.Int(),
                        O_Usato = c.String(),
                        O_ValUsato = c.Decimal(precision: 18, scale: 2),
                        O_ScontoPerc = c.Double(),
                        O_ProgDes = c.String(),
                        O_ProgVal = c.Decimal(precision: 18, scale: 2),
                        C_ValUsatoReal = c.Decimal(precision: 18, scale: 2),
                        O_CodPag = c.Int(),
                        O_Trasporto = c.String(),
                        O_Garanzia = c.String(),
                        O_Inst = c.String(),
                        N_PropNol = c.Boolean(),
                        N_LasciaTot = c.Boolean(),
                        N_Canone = c.Decimal(precision: 18, scale: 2),
                        N_Periodo = c.Int(),
                        N_Acconto = c.Decimal(precision: 18, scale: 2),
                        N_CopieCol = c.Int(),
                        N_CostoCopieCol = c.Decimal(precision: 18, scale: 2),
                        N_CopieBN = c.Int(),
                        N_CostoCopieBN = c.Decimal(precision: 18, scale: 2),
                        N_DurataContr = c.Int(),
                        N_PerFatt = c.Int(),
                        N_CopiePerFat = c.Int(),
                        N_Note = c.String(),
                        O_PrnTot = c.Boolean(),
                        IDComm = c.Int(),
                        IDReferente = c.Int(),
                        Descrizione = c.String(),
                        Reg = c.String(),
                        ModDoc = c.String(),
                        DataRich = c.DateTime(),
                        NumOrdCli = c.String(),
                        DatOrdCli = c.DateTime(),
                        C_cme_cod = c.Int(),
                        O_ScoPer = c.Boolean(),
                        F_CodArt = c.String(),
                        F_Qta = c.Int(),
                        F_Importo = c.Decimal(precision: 18, scale: 2),
                        F_Conf = c.Boolean(),
                        CodDestDiv = c.Int(),
                        ScoImp = c.Boolean(),
                        O_ScontoImp = c.Double(),
                        IDAssEff = c.Int(),
                        IDAssDif = c.Int(),
                        IDAssCau = c.Int(),
                        IDBan = c.Int(),
                        Progressivo = c.Int(),
                        Versione = c.Int(),
                        Revisione = c.Int(),
                        Cliente_Id = c.Int(),
                        Utente_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clienti", t => t.Cliente_Id)
                .ForeignKey("dbo.Utenti", t => t.Utente_Id)
                .Index(t => t.Cliente_Id)
                .Index(t => t.Utente_Id);
            
            CreateTable(
                "dbo.Clienti",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdCliente = c.Int(nullable: false),
                        RagSoc1 = c.String(),
                        RagSoc2 = c.String(),
                        Indirizzo = c.String(),
                        CAP = c.String(),
                        Localita = c.String(),
                        Prov = c.String(),
                        Tel = c.String(),
                        Fax = c.String(),
                        PIva = c.String(),
                        CF = c.String(),
                        EMail = c.String(),
                        HomePage = c.String(),
                        S_RagSoc1 = c.String(),
                        S_RagSoc2 = c.String(),
                        S_Indirizzo = c.String(),
                        S_CAP = c.String(),
                        S_Localita = c.String(),
                        S_Prov = c.String(),
                        Note = c.String(),
                        CodAge = c.String(),
                        CodPag = c.Int(),
                        Mitt = c.Boolean(),
                        Cell = c.String(),
                        IDTip = c.Int(),
                        BanApp = c.Int(),
                        IBAN = c.String(),
                        CCN = c.String(),
                        CIN = c.String(),
                        ZonCod = c.Int(),
                        DatUltMod = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Ruoli",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.RuoliUtente",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Ruoli", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Utenti", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Utenti",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nome = c.String(),
                        Cognome = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Ruolo_UserId = c.String(maxLength: 128),
                        Ruolo_RoleId = c.String(maxLength: 128),
                        Utilizzatore_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RuoliUtente", t => new { t.Ruolo_UserId, t.Ruolo_RoleId })
                .ForeignKey("dbo.Utilizzatori", t => t.Utilizzatore_Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => new { t.Ruolo_UserId, t.Ruolo_RoleId })
                .Index(t => t.Utilizzatore_Id);
            
            CreateTable(
                "dbo.AttestazioniUtente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Utenti", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.LoginUtente",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Utenti", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Utilizzatori",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                        RagioneSociale = c.String(nullable: false, maxLength: 200),
                        SynchroteamApiKey = c.String(),
                        SynchroteamAccount = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Utenti", "Utilizzatore_Id", "dbo.Utilizzatori");
            DropForeignKey("dbo.Utenti", new[] { "Ruolo_UserId", "Ruolo_RoleId" }, "dbo.RuoliUtente");
            DropForeignKey("dbo.RuoliUtente", "UserId", "dbo.Utenti");
            DropForeignKey("dbo.Offerte", "Utente_Id", "dbo.Utenti");
            DropForeignKey("dbo.LoginUtente", "UserId", "dbo.Utenti");
            DropForeignKey("dbo.AttestazioniUtente", "UserId", "dbo.Utenti");
            DropForeignKey("dbo.RuoliUtente", "RoleId", "dbo.Ruoli");
            DropForeignKey("dbo.VociOfferta", "Offerta_Id", "dbo.Offerte");
            DropForeignKey("dbo.Offerte", "Cliente_Id", "dbo.Clienti");
            DropForeignKey("dbo.VociOfferta", "Articolo_Id", "dbo.Articoli");
            DropIndex("dbo.LoginUtente", new[] { "UserId" });
            DropIndex("dbo.AttestazioniUtente", new[] { "UserId" });
            DropIndex("dbo.Utenti", new[] { "Utilizzatore_Id" });
            DropIndex("dbo.Utenti", new[] { "Ruolo_UserId", "Ruolo_RoleId" });
            DropIndex("dbo.Utenti", "UserNameIndex");
            DropIndex("dbo.RuoliUtente", new[] { "RoleId" });
            DropIndex("dbo.RuoliUtente", new[] { "UserId" });
            DropIndex("dbo.Ruoli", "RoleNameIndex");
            DropIndex("dbo.Offerte", new[] { "Utente_Id" });
            DropIndex("dbo.Offerte", new[] { "Cliente_Id" });
            DropIndex("dbo.VociOfferta", new[] { "Offerta_Id" });
            DropIndex("dbo.VociOfferta", new[] { "Articolo_Id" });
            DropTable("dbo.Utilizzatori");
            DropTable("dbo.LoginUtente");
            DropTable("dbo.AttestazioniUtente");
            DropTable("dbo.Utenti");
            DropTable("dbo.RuoliUtente");
            DropTable("dbo.Ruoli");
            DropTable("dbo.Clienti");
            DropTable("dbo.Offerte");
            DropTable("dbo.VociOfferta");
            DropTable("dbo.Articoli");
        }
    }
}
