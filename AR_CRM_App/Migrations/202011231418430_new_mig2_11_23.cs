namespace AR_CRM_App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new_mig2_11_23 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Offerte", "Cliente_Id", "dbo.Clienti");
            DropIndex("dbo.Offerte", new[] { "Cliente_Id" });
            AddColumn("dbo.Offerte", "Cliente_Id1", c => c.Int());
            AlterColumn("dbo.Offerte", "Cliente_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Offerte", "Cliente_Id1");
            AddForeignKey("dbo.Offerte", "Cliente_Id1", "dbo.Clienti", "Id");
            DropColumn("dbo.Offerte", "IdCliente");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Offerte", "IdCliente", c => c.Int(nullable: false));
            DropForeignKey("dbo.Offerte", "Cliente_Id1", "dbo.Clienti");
            DropIndex("dbo.Offerte", new[] { "Cliente_Id1" });
            AlterColumn("dbo.Offerte", "Cliente_Id", c => c.Int());
            DropColumn("dbo.Offerte", "Cliente_Id1");
            CreateIndex("dbo.Offerte", "Cliente_Id");
            AddForeignKey("dbo.Offerte", "Cliente_Id", "dbo.Clienti", "Id");
        }
    }
}
