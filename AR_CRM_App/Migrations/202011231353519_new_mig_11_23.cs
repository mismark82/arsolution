namespace AR_CRM_App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new_mig_11_23 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offerte", "IdCliente", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Offerte", "IdCliente");
        }
    }
}
