namespace AR_CRM_App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updates_2020_11_13 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Offerte", "IDCliOff", c => c.Int(nullable: false));
            AlterColumn("dbo.Offerte", "Progressivo", c => c.Int(nullable: false));
            AlterColumn("dbo.Offerte", "Versione", c => c.Int(nullable: false));
            AlterColumn("dbo.Offerte", "Revisione", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Offerte", "Revisione", c => c.Int());
            AlterColumn("dbo.Offerte", "Versione", c => c.Int());
            AlterColumn("dbo.Offerte", "Progressivo", c => c.Int());
            AlterColumn("dbo.Offerte", "IDCliOff", c => c.Int());
        }
    }
}
