﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AR_CRM_App.Models.View
{
    public class OffertaViewModelCreate
    {
        public Utente Utente { get; set; }
        public List<Cliente> Clienti { get; set; }
        public List<Articolo> Articoli { get; set; }
        public List<VoceOfferta> VociOfferta { get; set; }
        public int Id { get; set; }
        public Cliente Cliente { get; set; }
        public bool Accettata { get; set; }
        public bool Rifiutata { get; set; }
        public int Progressivo { get; set; }
        public int Versione { get; set; }
        public int Revisione { get; set; }
        public DateTime DataOff { get; set; }
        public DateTime Validita { get; set; }

    }

    public class OffertaViewModelListItem
    {
        public int Id { get; set; }
        public Cliente Cliente { get; set; }
        public bool Accettata { get; set; }
        public bool Rifiutata { get; set; }
        public int Progressivo { get; set; }
        public int Versione { get; set; }
        public int Revisione { get; set; }
        public DateTime DataOff { get; set; }
        public DateTime Validita { get; set; }
    }
    public class OffertaViewModelList
    { 
        public List<OffertaViewModelListItem> Offerte { get; set; }
        public Utente Utente { get; set; }


    }
}