﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AR_CRM_App.Models
{
    public class Utilizzatore
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Nome { get; set; }
        [Required]
        [MaxLength(200)]
        public string RagioneSociale { get; set; }
        public string SynchroteamApiKey { get; set; }
        public string SynchroteamAccount { get; set; }

        public virtual ICollection<Utente> Utenti { get; set; }

    }
}