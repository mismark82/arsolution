﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AR_CRM_App.Models
{
    public class Offerta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Offerta()
        {
            this.VociOfferta = new HashSet<VoceOfferta>();
        }


        [Key]
        public int Id { get; set; }
        [Display(Name = "Id Offerta")]


        public int Cliente_Id { get; set; }
        [Display(Name = "Destinatario")]


        [Required]
        public int IDCliOff { get; set; }
        public Nullable<int> IDCliPic { get; set; }
        public Nullable<System.DateTime> DataOff { get; set; }
        public Nullable<int> ProgOff { get; set; }
        public string Oggetto { get; set; }
        public string Intro { get; set; }
        public string Note { get; set; }
        public string Saluti { get; set; }
        public Nullable<System.DateTime> Validita { get; set; }
        public string Installazione { get; set; }
        public string ConsTempi { get; set; }
        public Nullable<bool> Accettata { get; set; }
        public Nullable<bool> Rifiutata { get; set; }
        public string MotivoRif { get; set; }
        public string PicNomPrev { get; set; }
        public Nullable<int> PicNumPrev { get; set; }
        public Nullable<System.DateTime> PicDataPrev { get; set; }
        public Nullable<System.DateTime> C_Consegna { get; set; }
        public string C_SpedMez { get; set; }
        public string C_Imballo { get; set; }
        public string C_Pagamento { get; set; }
        public string C_Banca { get; set; }
        public string C_RifSig { get; set; }
        public string C_Note { get; set; }
        public string O_KitInstDes { get; set; }
        public Nullable<decimal> O_KitInst { get; set; }
        public Nullable<bool> O_InstForfait { get; set; }
        public Nullable<decimal> O_InstTrasp { get; set; }
        public Nullable<int> O_InstOrePrev { get; set; }
        public string O_Usato { get; set; }
        public Nullable<decimal> O_ValUsato { get; set; }
        public Nullable<double> O_ScontoPerc { get; set; }
        public string O_ProgDes { get; set; }
        public Nullable<decimal> O_ProgVal { get; set; }
        public Nullable<decimal> C_ValUsatoReal { get; set; }
        public Nullable<int> O_CodPag { get; set; }
        public string O_Trasporto { get; set; }
        public string O_Garanzia { get; set; }
        public string O_Inst { get; set; }
        public Nullable<bool> N_PropNol { get; set; }
        public Nullable<bool> N_LasciaTot { get; set; }
        public Nullable<decimal> N_Canone { get; set; }
        public Nullable<int> N_Periodo { get; set; }
        public Nullable<decimal> N_Acconto { get; set; }
        public Nullable<int> N_CopieCol { get; set; }
        public Nullable<decimal> N_CostoCopieCol { get; set; }
        public Nullable<int> N_CopieBN { get; set; }
        public Nullable<decimal> N_CostoCopieBN { get; set; }
        public Nullable<int> N_DurataContr { get; set; }
        public Nullable<int> N_PerFatt { get; set; }
        public Nullable<int> N_CopiePerFat { get; set; }
        public string N_Note { get; set; }
        public Nullable<bool> O_PrnTot { get; set; }
        public Nullable<int> IDComm { get; set; }
        public Nullable<int> IDReferente { get; set; }
        public string Descrizione { get; set; }
        public string Reg { get; set; }
        public string ModDoc { get; set; }
        public Nullable<System.DateTime> DataRich { get; set; }
        public string NumOrdCli { get; set; }
        public Nullable<System.DateTime> DatOrdCli { get; set; }
        public Nullable<int> C_cme_cod { get; set; }
        public Nullable<bool> O_ScoPer { get; set; }
        public string F_CodArt { get; set; }
        public Nullable<int> F_Qta { get; set; }
        public Nullable<decimal> F_Importo { get; set; }
        public Nullable<bool> F_Conf { get; set; }
        public Nullable<int> CodDestDiv { get; set; }
        public Nullable<bool> ScoImp { get; set; }
        public Nullable<double> O_ScontoImp { get; set; }
        public Nullable<int> IDAssEff { get; set; }
        public Nullable<int> IDAssDif { get; set; }
        public Nullable<int> IDAssCau { get; set; }
        public Nullable<int> IDBan { get; set; }

        public virtual Cliente Cliente{ get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<VoceOfferta> VociOfferta { get; set; }


        // Custom fields

        [Required]
        public int Progressivo { get; set; }
        [Required]
        public int  Versione { get; set; }
        [Required]
        public int Revisione { get; set; }

    }
}