﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AR_CRM_App.Models
{
    public class Utente : IdentityUser
    {

        public string Nome { get; set; }
        public string Cognome { get; set; }
        public virtual IdentityUserRole Ruolo { get; set; }
        public virtual Utilizzatore Utilizzatore { get; set; }
        public virtual ICollection<Offerta> Offerte { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Utente> manager, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            // Tenere presente che il valore di authenticationType deve corrispondere a quello definito in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Aggiungere qui i reclami utente personalizzati
            return userIdentity;
        }
    }
}