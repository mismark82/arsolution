﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AR_CRM_App.Models;

namespace AR_CRM_App.Controllers
{
    [Authorize]
    public class UtilizzatoriController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Utilizzatori
        public ActionResult Index()
        {
            return View(db.Utilizzatori.ToList());
        }

        // GET: Utilizzatori/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utilizzatore utilizzatore = db.Utilizzatori.Find(id);
            if (utilizzatore == null)
            {
                return HttpNotFound();
            }
            return View(utilizzatore);
        }

        // GET: Utilizzatori/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Utilizzatori/Create
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome,RagioneSociale,SynchroteamApiKey,SynchroteamAccount")] Utilizzatore utilizzatore)
        {
            if (ModelState.IsValid)
            {
                db.Utilizzatori.Add(utilizzatore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(utilizzatore);
        }

        // GET: Utilizzatori/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utilizzatore utilizzatore = db.Utilizzatori.Find(id);
            if (utilizzatore == null)
            {
                return HttpNotFound();
            }
            return View(utilizzatore);
        }

        // POST: Utilizzatori/Edit/5
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome,RagioneSociale,SynchroteamApiKey,SynchroteamAccount")] Utilizzatore utilizzatore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(utilizzatore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(utilizzatore);
        }

        // GET: Utilizzatori/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utilizzatore utilizzatore = db.Utilizzatori.Find(id);
            if (utilizzatore == null)
            {
                return HttpNotFound();
            }
            return View(utilizzatore);
        }

        // POST: Utilizzatori/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Utilizzatore utilizzatore = db.Utilizzatori.Find(id);
            db.Utilizzatori.Remove(utilizzatore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
