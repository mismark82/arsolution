﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AR_CRM_App.Models;

namespace AR_CRM_App.Controllers.Api
{
    public class ArticoliController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Articoli
        public IQueryable<Articolo> GetArticoli()
        {
            return db.Articoli;
        }

        // GET: api/Articoli/5
        [ResponseType(typeof(Articolo))]
        public IHttpActionResult GetArticolo(int id)
        {
            Articolo articolo = db.Articoli.Find(id);
            if (articolo == null)
            {
                return NotFound();
            }

            return Ok(articolo);
        }

        // PUT: api/Articoli/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutArticolo(int id, Articolo articolo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != articolo.Id)
            {
                return BadRequest();
            }

            db.Entry(articolo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArticoloExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Articoli
        [ResponseType(typeof(Articolo))]
        public IHttpActionResult PostArticolo(Articolo articolo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Articoli.Add(articolo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = articolo.Id }, articolo);
        }

        // DELETE: api/Articoli/5
        [ResponseType(typeof(Articolo))]
        public IHttpActionResult DeleteArticolo(int id)
        {
            Articolo articolo = db.Articoli.Find(id);
            if (articolo == null)
            {
                return NotFound();
            }

            db.Articoli.Remove(articolo);
            db.SaveChanges();

            return Ok(articolo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArticoloExists(int id)
        {
            return db.Articoli.Count(e => e.Id == id) > 0;
        }
    }
}