﻿using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AR_CRM_App.Models;
using AR_CRM_App.Models.View;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System;

namespace AR_CRM_App.Controllers
{
    [Authorize]
    public class OfferteController : Controller
    {
        #region Funzioni di base e proprietà
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public OfferteController()
        {
        }

        public OfferteController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private ApplicationDbContext db = new ApplicationDbContext();
        #endregion

        #region Index
        // GET: Offerte
        public ActionResult Index()
        {
            // trasformo una offerta in un modello più semplice da gestire...
            List <OffertaViewModelListItem> list = new List<OffertaViewModelListItem>();
            foreach (Offerta offerta in db.Offerte.ToList())
            {
                list.Add(ToListItem(offerta));
            }
            return View(list);
        }
        #endregion

        #region Details
        // GET: Offerte/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offerta offerta = db.Offerte.Find(id);
            if (offerta == null)
            {
                return HttpNotFound();
            }
            return View(offerta);
        }
        #endregion

        #region Create
        // GET: Offerte/Create
        public ActionResult Create()
        {
            var clienti = db.Clienti.ToList();
            var articoli = db.Articoli.ToList();
            var utente = UserManager.FindByIdAsync(User.Identity.GetUserId()).Result;

            ViewBag.Clienti = clienti;
            ViewBag.Articoli = articoli;
            ViewBag.Utente = utente;



            return View();
        }

        // POST: Offerte/Create
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Cliente_Id,DataOff,Oggetto,Intro,Note,Saluti,Validita,Installazione,ConsTempi,Accettata,Rifiutata,Progressivo,Versione,Revisione")] Offerta offerta)
        {
            var lastProgressivo = db.Offerte.Max(o => o.Progressivo);
            if (lastProgressivo >= 0)
                offerta.Progressivo = lastProgressivo + 1;
            else
                offerta.Progressivo = 1;
            offerta.Versione = 1;
            offerta.Revisione = 0;

            if (ModelState.IsValid)
            {
                offerta.Cliente = db.Clienti.Find(offerta.Cliente_Id);

                db.Offerte.Add(offerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(offerta);
        }

        #endregion

        #region Edit
        // GET: Offerte/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offerta offerta = db.Offerte.Find(id);
            if (offerta == null)
            {
                return HttpNotFound();
            }
            return View(offerta);
        }

        // POST: Offerte/Edit/5
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Cliente_Id,DataOff,ProgOff,Oggetto,Intro,Note,Saluti,Validita,Installazione,ConsTempi,Accettata,Rifiutata,Progressivo,Versione,Revisione")] Offerta offerta)
        {
            if (ModelState.IsValid)
            {
                var Progressivo = db.Offerte.Find(offerta.Id).Progressivo;
                var Revisione = db.Offerte.Where(t => t.Progressivo == Progressivo).Max(t => t.Revisione);
                var Versione = db.Offerte.Where(t => t.Progressivo == Progressivo).Max(t => t.Versione);


                // siccome è una edit, il progressivo non può cambiare
                offerta.Progressivo = Progressivo;

                if (offerta.Revisione > Revisione)
                {
                    offerta.Revisione = Revisione + 1;
                    offerta.Versione = 1;
                }
                else
                {
                    offerta.Revisione = db.Offerte.Find(offerta.Id).Revisione;
                    offerta.Versione = Versione + 1;
                }

             
                db.Offerte.Add(offerta);
                //db.Entry(offerta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(offerta);
        }

        #endregion

        #region Delete
        // GET: Offerte/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offerta offerta = db.Offerte.Find(id);
            if (offerta == null)
            {
                return HttpNotFound();
            }
            return View(offerta);
        }

        // POST: Offerte/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Offerta offerta = db.Offerte.Find(id);
            db.Offerte.Remove(offerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion

        #region Metodi DTO
        private static OffertaViewModelListItem ToListItem(Offerta offerta)
        {
            var item = new OffertaViewModelListItem();
            item.Id = offerta.Id;
            item.Progressivo = offerta.Progressivo;
            item.Revisione = offerta.Revisione;
            item.Versione = offerta.Versione;

            if (offerta.Rifiutata != null)
                item.Rifiutata = offerta.Rifiutata.Value;

            if (offerta.Accettata != null)
                item.Accettata = offerta.Accettata.Value;

            item.Cliente = offerta.Cliente;
            if (offerta.DataOff != null)
                item.DataOff = offerta.DataOff.Value;

            if (offerta.Validita != null)
                item.Validita = offerta.Validita.Value;
            return item;
        }
        #endregion
    }
}
