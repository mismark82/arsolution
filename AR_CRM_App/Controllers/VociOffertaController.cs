﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AR_CRM_App.Models;

namespace AR_CRM_App.Controllers
{
    [Authorize]
    public class VociOffertaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: VociOfferta
        public ActionResult Index()
        {
            return View(db.VoceOffertas.ToList());
        }

        // GET: VociOfferta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VoceOfferta voceOfferta = db.VoceOffertas.Find(id);
            if (voceOfferta == null)
            {
                return HttpNotFound();
            }
            return View(voceOfferta);
        }

        // GET: VociOfferta/Create
        public ActionResult Create(int idOfferta)
        {
            ViewBag.offerta = db.Offerte.Find(idOfferta); 
            return View();
        }

        // POST: VociOfferta/Create
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdOfferta,TipRig,IdArticolo,Titolo,DesOff,Qta,Prezzo,C_Prezzo,Listino,Costo,IDForn,DataArrivo,DataCons,OrdVoce,ScoPer,Conf,PathFoto,IDMacchina,UM,GruMer,BatLun,BatLar,BatSpe,BatPS,BatPreKG,BatPerAum,BatPreLav,CodArt,CodMarca,Forecast,InAss,NumMac,NumOre,IDReparto,CostoOrario")] VoceOfferta voceOfferta)
        {
            if (ModelState.IsValid)
            {
                db.VoceOffertas.Add(voceOfferta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(voceOfferta);
        }

        // GET: VociOfferta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VoceOfferta voceOfferta = db.VoceOffertas.Find(id);
            if (voceOfferta == null)
            {
                return HttpNotFound();
            }
            return View(voceOfferta);
        }

        // POST: VociOfferta/Edit/5
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdOfferta,TipRig,IdArticolo,Titolo,DesOff,Qta,Prezzo,C_Prezzo,Listino,Costo,IDForn,DataArrivo,DataCons,OrdVoce,ScoPer,Conf,PathFoto,IDMacchina,UM,GruMer,BatLun,BatLar,BatSpe,BatPS,BatPreKG,BatPerAum,BatPreLav,CodArt,CodMarca,Forecast,InAss,NumMac,NumOre,IDReparto,CostoOrario")] VoceOfferta voceOfferta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(voceOfferta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(voceOfferta);
        }

        // GET: VociOfferta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VoceOfferta voceOfferta = db.VoceOffertas.Find(id);
            if (voceOfferta == null)
            {
                return HttpNotFound();
            }
            return View(voceOfferta);
        }

        // POST: VociOfferta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VoceOfferta voceOfferta = db.VoceOffertas.Find(id);
            db.VoceOffertas.Remove(voceOfferta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
